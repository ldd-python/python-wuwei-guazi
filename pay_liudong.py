#!/usr/bin/env python
# coding:utf-8
import uuid
import datetime
import json
import requests
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import MD5
import base64


contractNo = "1fb2501e-1031-11e9-befb-3c15c2e63532"
# class pay_liudong:
now_time = datetime.datetime.now().strftime('%Y-%m-%d')

# 公私钥配置
merid = 'systemdeduct'
secret = '628c8b28bb6fdf5c5add6f18da47f1a6'
privatekey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOXUGOKdEkssOI1zqNw4pb6emH1o1JYxooyTQ7FN1IBNqTJLuvA3GsswXIkuQj0imce6ywG/XOCwc9R1l5FwcORtwx2FihGCl7eBkhUwnT0EwGOEARPh96ey+TfvsvRaHOn672v1TEhajAftgm4l7fJDtHdGBjHOs+5Mlir9Z65RAgMBAAECgYEArtAiUZR5yrYLGgTEhyWLZK+Le7CWKtv8MQL+tUlm/mST8s7JlVfEyJKzgCCwf4HnCJXbPiwJgFqW8B61uAmXw6cEoPftEnzvKBTyISt/iEf7DTWKGkDBnlYM9sFU6pU61jw17XEDQRtSBG6cfrlGSelqf25+c8onxu4YwTeLH/ECQQD/H69tPy0FYRvCJ5yXdXEVCKshNN01P+UdDzGtyysE/gmpalbewT+uznApa0qFntcYb8eSpUJzrUlItSCBGUpdAkEA5p4r3qF+4g5V7MBHm3+v1l9JKxYK76990AQJa122rfkY2EEVuvU+8KIAQpVflu/HpDe8QH4mQZTsZj24Skt8hQJAL5j2vrgRqzZB2ohPY8aKcXUrkEdvmdaw5SoHh7gm74iBvvTS/j4ppnBnZqLYxXMsCCgaoNZqPnCvAnygctWIFQJAHm2KLkKyohLwJV+tUwgC5E8IMWYkJUHLYNHXiFICE2xFaesUeel313oYfLCGvzx9493yubOrSoXitw63rR3OnQJBALwGSnGYodmJB5k7un0X6LlO4nSu/+SX9lweloZ1AUg15IuCIYxHAFKwOtOJmx/eMcITaLq8l1qzZ907UXY+Mfs='
pubKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDl1BjinRJLLDiNc6jcOKW+nph9aNSWMaKMk0OxTdSATakyS7rwNxrLMFyJLkI9IpnHussBv1zgsHPUdZeRcHDkbcMdhYoRgpe3gZIVMJ09BMBjhAET4fensvk377L0Whzp+u9r9UxIWowH7YJuJe3yQ7R3RgYxzrPuTJYq/WeuUQIDAQAB'
url = "http://192.168.0.213:7778/api/v2/repaymentOrder"

def sign(data):
    key = RSA.importKey(base64.b64decode(privatekey))
    signer = PKCS1_v1_5.new(key)
    h = MD5.new(data)
    signature = signer.sign(h)
    return base64.b64encode(signature)



def importAssets(contractNos):

    financialProductCode ="SXPPDtest" #SXPPDtest

    # python字典，类似于map
    data = {
        "requestNo":str(uuid.uuid1()),
        "orderUniqueId": str(uuid.uuid1()),
        "transType": 0,
        "financialContractNo": str(financialProductCode),
        "orderType": 1,
        "orderAmount": 20000,

        "paymentOrderDetail":json.dumps([{
            "contractNo":str(contractNos),
            "paymentWay":'101',
            "detailsTotalAmount":20000,
            "payScheduleNo":str(uuid.uuid1()),
            "PaymentRemard": "商户备注",
            "paymentAmountFeeType":"30000.101.1002"
        }])
    }

    print(json.dumps(data))
    print('**************************')
    # 签名
    lst = []
    for key, value in data.items():
        # "" if value==None else value
        param = (str(key) + '=' + str(value) + '&')
        lst.append(param)
    lst.sort(key=None, reverse=False)
    str_data = str(''.join(lst))        #'-'.join(list): 把list的元素用'-' 连接起来
    str_data = str_data[0:-1]
    print(str_data)
    sign_data = sign(str_data.encode('utf-8'))
    print(sign_data.decode())
    headers = {'merId': str(merid),
               'secret': str(secret),
               'sign': sign_data.decode(),
               'Connection': 'Keep-alive',
               'Content-Type': 'application/x-www-form-urlencoded'}
    results = requests.post(url, data=data, headers=headers) #.text:获取相应内容
    res=results.text.encode('utf-8')
    print(res)

# p=pay_liudong()#初始化类实例=>p
# p.importAssets(contractNo) #使用实例 p 调用类方法