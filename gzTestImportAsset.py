# -*- coding:UTF-8 -*-
import json
import uuid
import requests
import datetime
from dateutil.relativedelta import relativedelta
import cgi, base64
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import MD5

# #公私钥配置
# merid = 't_test_wb'
# secret = '1234567890'
# privatekey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMFVnFHs8019jldQYeXseUoILwdhOiK3eJZcu5bqAKAvqelR5NdhwgqhTRQ7R568lPA/5fSx3DdTB0fzDnxMdK2od3IljEdTExUl7QSE6UCtbtsgQTHgFJci3m4k7UdblMpPZsSwfJ8KI6gCAGZNr44X/ZBy/5MdWOlevXbsbNiTAgMBAAECgYAOghQm+bcQW5mw57FRBbvcWFzfGua38Di2X+Mb4heF7c28Wo7nsshS7+PO3tvzB4fNt53UDPzPkgV72rt9jGc892L694fMyjivkpFQjqGy9lzQybW1BL0Iq0qWP2wiTfwQgOCgNjqt5YpYqYDs7YocNShXBxfPe6kw6x01A8UMoQJBAPIit8/9oyC1vGddzB0AUw372KdaNa0ep5kctAlGIXzFGUUtYLTCaPFobh1oWH0uJx/tlDDI2UyX0IfO+MWTwwsCQQDMZ42KnK3gqurYikTMJuSqma1+SkQCjEwtPni6ZZMSozZaOVV3xufmifkKt/4R71pOVsTMh2EKFRTeG8XUtDWZAkAN2tq1tgetiXicWYuiZTBbbnu5J7pGi6h/ZFsQ5MhpwUVu5efQsAcdOSKD4EKf9xIaC5gdKoD0lwNBoiAt/gR9AkEAk+4lq7l0xXItAIYhADbtquIJg6qvycBLqTrbGNtGM2jXfzibOFJFpr0X9b/vKn+1NZjqWLNpcWiMb6pd5HB/kQJBANRAcQzkP3S0sghHkC+qkX106Gmuongm8Av+Vi5Sfub13HkTvvoXKYsVKjy/inzaolkP7Fv1k60MmOHGPBAa6/o='
# pubKey = '''-----BEGIN PUBLIC KEY-----
# MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBVZxR7PNNfY5XUGHl7HlKCC8H
# YToit3iWXLuW6gCgL6npUeTXYcIKoU0UO0eevJTwP+X0sdw3UwdH8w58THStqHdy
# JYxHUxMVJe0EhOlArW7bIEEx4BSXIt5uJO1HW5TKT2bEsHyfCiOoAgBmTa+OF/2Q
# cv+THVjpXr127GzYkwIDAQAB
# -----END PUBLIC KEY-----'''



#公私钥配置
#merid = 't_test_zfb'
#secret = '123456'
merid = 'systemdeduct'
secret = '628c8b28bb6fdf5c5add6f18da47f1a6'
privatekey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOXUGOKdEkssOI1zqNw4pb6emH1o1JYxooyTQ7FN1IBNqTJLuvA3GsswXIkuQj0imce6ywG/XOCwc9R1l5FwcORtwx2FihGCl7eBkhUwnT0EwGOEARPh96ey+TfvsvRaHOn672v1TEhajAftgm4l7fJDtHdGBjHOs+5Mlir9Z65RAgMBAAECgYEArtAiUZR5yrYLGgTEhyWLZK+Le7CWKtv8MQL+tUlm/mST8s7JlVfEyJKzgCCwf4HnCJXbPiwJgFqW8B61uAmXw6cEoPftEnzvKBTyISt/iEf7DTWKGkDBnlYM9sFU6pU61jw17XEDQRtSBG6cfrlGSelqf25+c8onxu4YwTeLH/ECQQD/H69tPy0FYRvCJ5yXdXEVCKshNN01P+UdDzGtyysE/gmpalbewT+uznApa0qFntcYb8eSpUJzrUlItSCBGUpdAkEA5p4r3qF+4g5V7MBHm3+v1l9JKxYK76990AQJa122rfkY2EEVuvU+8KIAQpVflu/HpDe8QH4mQZTsZj24Skt8hQJAL5j2vrgRqzZB2ohPY8aKcXUrkEdvmdaw5SoHh7gm74iBvvTS/j4ppnBnZqLYxXMsCCgaoNZqPnCvAnygctWIFQJAHm2KLkKyohLwJV+tUwgC5E8IMWYkJUHLYNHXiFICE2xFaesUeel313oYfLCGvzx9493yubOrSoXitw63rR3OnQJBALwGSnGYodmJB5k7un0X6LlO4nSu/+SX9lweloZ1AUg15IuCIYxHAFKwOtOJmx/eMcITaLq8l1qzZ907UXY+Mfs='
pubKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDl1BjinRJLLDiNc6jcOKW+nph9aNSWMaKMk0OxTdSATakyS7rwNxrLMFyJLkI9IpnHussBv1zgsHPUdZeRcHDkbcMdhYoRgpe3gZIVMJ09BMBjhAET4fensvk377L0Whzp+u9r9UxIWowH7YJuJe3yQ7R3RgYxzrPuTJYq/WeuUQIDAQAB'
#merid ='zhxt-002'
#secret ='8b1ed4e3-5291-11e8-bb37-5254007c50f5'
#privatekey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKpOCnHnHiODB9jSjBoj6uQikzKJF6RMplYKXHC1hP1nNwPDB179tCGslY8SLr+Yf1dZgVfcE7gZxgLQt09lIwFxUw1ItucXFSkYHkDjWHWrFLoq0WJ4/LoO3zRtYX28rGO4esWl75eVmJoxXngEDObHwN6fZmyJDtacK9J7OvsRAgMBAAECgYABczSnj540EzOH5xMGTfP8CUdTkNNAtN5dsSzkUMdXGP/g6lIvvt0/xCZp12/KddPvvHVbeCY61Q1YjMpixKutndpcirzBnkGZgvaAC2mfT1HyyHOTMVZcXFUrcq0A++K3XGzqALoeCSwVXEfkcitQotLZb6C59c2Rm85RjiGo+QJBAPn8q4ywROssCrvwnM6xucV/GKcHvauKVv0GkEzdcDUYRdCivSx927n34ZeMWSXuOG2mSxUyG4OMgIPadTKG8gcCQQCuZrd7M25Szp2kqQy5DECC0/pVvsvW08mEwc18+337VDupUso+qkXRettNFpq121ReApm+bk/cb1ZhwZmi/AQnAkB55ehs+QMSPe6aCFEeRPGWSUxnHlIxlIqoZQv2P0BY05Yvjzt5t4FuZF4fvPM3mLrxVNZl5oIeGmQMZ3kgg0b/AkAjI9UygMFo8/K1m85RdtOEyNt27XO6/H9tPXN/DFltC6Ld0i2oBLz+oWeQl85ieCf+rT13LljvDIv2NIEVYvwfAkBkLnuTtrxEaiYF6VP2LDRWUEfvS4Lis2w13FIdi2soOwA78CmNqpmoNsAvsZRP8oE4lhrFSNym2nwq4lA43ojB"
#pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqTgpx5x4jgwfY0owaI+rkIpMyiRekTKZWClxwtYT9ZzcDwwde/bQhrJWPEi6/mH9XWYFX3BO4GcYC0LdPZSMBcVMNSLbnFxUpGB5A41h1qxS6KtFiePy6Dt80bWF9vKxjuHrFpe+XlZiaMV54BAzmx8Den2ZsiQ7WnCvSezr7EQIDAQAB"
# 签名
def sign(data):
    key = RSA.importKey(base64.b64decode(privatekey))
    h = MD5.new(data)
    signer = PKCS1_v1_5.new(key)
    signature = signer.sign(h)
    return base64.b64encode(signature)

# 验签
def verify(data, signature):
    key = RSA.importKey(pubKey)
    h = MD5.new(data)
    verifier = PKCS1_v1_5.new(key)
    if verifier.verify(h, base64.b64decode(signature)):
        return True
    return False
date_now = datetime.datetime.now().strftime('%Y-%m-%d')
date_next_month = (datetime.datetime.now() + relativedelta(months=+1)).strftime('%Y-%m-%d')
date_next_next_month = (datetime.datetime.now() + relativedelta(months=+2)).strftime('%Y-%m-%d')
two_out_url = "http://tardis.5veda.net/contra_nginx/api/v3/importAssetPackage"
neiwang_url = "http://192.168.69.84:7778/api/v3/importAssetPackage"
#瓜子导入资产包
def gzTestImportAsset():
    lst = []
    uniqueId = uuid.uuid1()
    totalAmount = "3000"
    Amount = '3000'
    financialProductCode ="SXPPDtest" #SXPPDtest
    date_now = datetime.datetime.now().strftime('%Y-%m-%d')
    # date_next_month = (datetime.datetime.now() + relativedelta(months=+1)).strftime('%Y-%m-%d')
    # date_next_next_month = (datetime.datetime.now() + relativedelta(months=+2)).strftime('%Y-%m-%d')
    print (date_next_month)
    print (date_next_next_month)

    pythonModel = {
        "financialProductCode":str(financialProductCode),
        "thisBatchContractsTotalNumber":1,
        "thisBatchContractsTotalAmount":str(totalAmount),
        "contractDetails":[{
            "uniqueId":str(uniqueId),
            "loanContractNo":str(uniqueId),
            "loanCustomerNo":str(uuid.uuid1()),
            "subjectMatterassetNo":"",
            "loanCustomerName":"瓜子",
            "iDCardNo":"522227197109237460",
            "bankCode":"C10308",
            "bankOfTheProvince":"",#必填
            "bankOfTheCity":"",#必填
            "repaymentAccountNo":"6222021208017891440",
            "loanTotalAmount":str(totalAmount),
            "loanPeriods":1,
            "effectDate":"2018-12-21",
            "expiryDate":"2018-12-23",
            "loanRates":"4.38",
            "interestRateCycle":"3",
            "penalty": "6.57",
            "repaymentWay": "1",
            "repaymentPlanDetails":[{
                "repaymentDate": str(date_now),
                "repaymentPrincipal": Amount,
                "repaymentInterest": "10",
                "techMaintenanceFee": "10",
                "loanServiceFee": "10",
                "otheFee": "10"}
                # ,
                # {"repaymentDate": date_next_month,
                #  "repaymentPrincipal": Amount,
                #  "repaymentInterest": "10",
                #  "techMaintenanceFee": "10",
                #  "loanServiceFee": "10",
                #  "otheFee": "10"},
                # {"repaymentDate": date_next_next_month,
                #  "repaymentPrincipal": Amount,
                #  "repaymentInterest": "10",
                #  "techMaintenanceFee": "10",
                #  "loanServiceFee": "10",
                #  "otheFee": "10"}
            ],
            "contractStandard":2,
            "extraInfos":[{
                "code":"2.001",
                "content":u"100000.00"
            },
                {"code": "2.002",
                 "content": u"120000.00"},
                {"code": "2.003",
                 "content": u"卖家"},
                {"code": "2.004",
                 "content": u"339005199812119656"},
                {"code": "2.006",
                 "content": u"622222222222589650"},
                {"code": "2.005",
                 "content": u"卖车的人"},
                {"code": "2.007",
                 "content": u"C10102"},
                {"code": "2.008",
                 "content": u"0"}]




        }]
    }

    data = {
        "requestNo":str(uuid.uuid1()),
        "importAssetPackageContent":json.dumps(pythonModel)
    }



    #print data
    for key, value in data.items():
        "" if value == None else value
        param = str(key) + '=' + str(value) + '&'
        lst.append(param)
    lst.sort(key=None, reverse=False)
    str_data = str(''.join(lst))        #'-'.join(list): 把list的元素用'-' 连接起来
    str_data = str_data[0:-1]
    print (str_data.decode("unicode_escape"))
    sign_data =sign(str_data)
    print (sign_data)
    headers = {'merId': str(merid),
               'secret': str(secret),
               'sign': str(sign_data),
               'Connection': 'Keep-alive',
               'Conntent-Type': 'application/x-www-form-urlencoded',
               'User-Agent': 'Apache-HttpClient/4.2.6(java 1.5)'}
    results = requests.post(neiwang_url, data, headers=headers).text
    responses = []
    responses.append(results)
    print (results)

gzTestImportAsset()