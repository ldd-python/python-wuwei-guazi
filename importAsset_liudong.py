#!/usr/bin/env python3
# coding:utf-8
import uuid
import datetime
import json
import requests
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import MD5
import base64
from guazi.pay_liudong import importAssets
import time
# pay_liudong.path.append(r'/home/liudong/workspace/python/guazi')


now_time = datetime.datetime.now().strftime('%Y-%m-%d')

# 公私钥配置
merid = 'systemdeduct'
secret = '628c8b28bb6fdf5c5add6f18da47f1a6'
privatekey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOXUGOKdEkssOI1zqNw4pb6emH1o1JYxooyTQ7FN1IBNqTJLuvA3GsswXIkuQj0imce6ywG/XOCwc9R1l5FwcORtwx2FihGCl7eBkhUwnT0EwGOEARPh96ey+TfvsvRaHOn672v1TEhajAftgm4l7fJDtHdGBjHOs+5Mlir9Z65RAgMBAAECgYEArtAiUZR5yrYLGgTEhyWLZK+Le7CWKtv8MQL+tUlm/mST8s7JlVfEyJKzgCCwf4HnCJXbPiwJgFqW8B61uAmXw6cEoPftEnzvKBTyISt/iEf7DTWKGkDBnlYM9sFU6pU61jw17XEDQRtSBG6cfrlGSelqf25+c8onxu4YwTeLH/ECQQD/H69tPy0FYRvCJ5yXdXEVCKshNN01P+UdDzGtyysE/gmpalbewT+uznApa0qFntcYb8eSpUJzrUlItSCBGUpdAkEA5p4r3qF+4g5V7MBHm3+v1l9JKxYK76990AQJa122rfkY2EEVuvU+8KIAQpVflu/HpDe8QH4mQZTsZj24Skt8hQJAL5j2vrgRqzZB2ohPY8aKcXUrkEdvmdaw5SoHh7gm74iBvvTS/j4ppnBnZqLYxXMsCCgaoNZqPnCvAnygctWIFQJAHm2KLkKyohLwJV+tUwgC5E8IMWYkJUHLYNHXiFICE2xFaesUeel313oYfLCGvzx9493yubOrSoXitw63rR3OnQJBALwGSnGYodmJB5k7un0X6LlO4nSu/+SX9lweloZ1AUg15IuCIYxHAFKwOtOJmx/eMcITaLq8l1qzZ907UXY+Mfs='
pubKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDl1BjinRJLLDiNc6jcOKW+nph9aNSWMaKMk0OxTdSATakyS7rwNxrLMFyJLkI9IpnHussBv1zgsHPUdZeRcHDkbcMdhYoRgpe3gZIVMJ09BMBjhAET4fensvk377L0Whzp+u9r9UxIWowH7YJuJe3yQ7R3RgYxzrPuTJYq/WeuUQIDAQAB'
url = "http://192.168.0.213:7778/api/v3/importAssetPackage"

def sign(data):
    key = RSA.importKey(base64.b64decode(privatekey))
    signer = PKCS1_v1_5.new(key)
    h = MD5.new(data)
    signature = signer.sign(h)
    return base64.b64encode(signature)


uniqueId = uuid.uuid1()
def importAsset():

    totalAmount = "3000"
    Amount = '3000'
    financialProductCode ="SXPPDtest" #SXPPDtest

    # python字典，类似于map
    pythonModel = {
        "financialProductCode": str(financialProductCode),#str(object)函数：将object转换成string
        "thisBatchContractsTotalNumber": 1,
        "thisBatchContractsTotalAmount": str(totalAmount),
        "contractDetails": [{
            "uniqueId": str(uniqueId),
            "loanContractNo": str(uniqueId),
            "loanCustomerNo": str(uuid.uuid1()),
            "subjectMatterassetNo": "",
            "loanCustomerName": u"瓜子",
            "iDCardNo": "522227197109237460",
            "bankCode": "C10308",
            "bankOfTheProvince": "",#必填
            "bankOfTheCity": "",  #必填
            "repaymentAccountNo": "6222021208017891440",
            "loanTotalAmount": str(totalAmount),
            "loanPeriods": 1,
            "effectDate": "2018-12-21",
            "expiryDate": "2018-12-23",
            "loanRates": "4.38",
            "interestRateCycle": "0",
            "penalty": "6.57",
            "repaymentWay": "1",
            "repaymentPlanDetails": [{
                "repaymentDate": now_time,
                "repaymentPrincipal": Amount,
                "repaymentInterest": "10",
                "techMaintenanceFee": "10",
                "loanServiceFee": "10",
                "otheFee": "10"}
            ],
            "contractStandard": 2,
            "extraInfos": [
                {"code": "2.001",
                "content":u"100000.00"},
                {"code": "2.002",
                 "content": u"120000.00"},
                {"code": "2.003",
                 "content": u"卖家"},
                {"code": "2.004",
                 "content": u"339005199812119656"},
                {"code": "2.006",
                 "content": u"622222222222589650"},
                {"code": "2.005",
                 "content": u"卖车的人"},
                {"code": "2.007",
                 "content": u"C10102"},
                {"code": "2.008",
                 "content": u"0"}
            ]

        }]
    }

    data = {
        "requestNo": str(uuid.uuid1()),
        "importAssetPackageContent": json.dumps(pythonModel)
    }

    print(json.dumps(pythonModel))
    print('**************************')
    # 签名
    lst = []
    for key, value in data.items():
        # "" if value==None else value
        param = (str(key) + '=' + str(value) + '&')
        lst.append(param)
    lst.sort(key=None, reverse=False)
    str_data = str(''.join(lst))        #'-'.join(list): 把list的元素用'-' 连接起来
    str_data = str_data[0:-1]
    print(str_data)
    sign_data = sign(str_data.encode('utf-8'))
    sign_data = sign_data.decode('utf-8')
    print(sign_data)
    headers = {'merId': str(merid),
               'secret': str(secret),
               'sign': sign_data,
               'Connection': 'Keep-alive',
               'Content-Type': 'application/x-www-form-urlencoded'}
    results = requests.post(url, data=data, headers=headers) #.text:获取相应内容
    res=results.text.encode('utf-8')
    print(res)

importAsset()#导入资产包
print ("Start : %s" % time.ctime())
time.sleep(5)#睡眠10s
print ("End : %s" % time.ctime())
importAssets(uniqueId)#支付